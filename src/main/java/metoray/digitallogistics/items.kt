package metoray.digitallogistics

import net.minecraft.creativetab.CreativeTabs
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.Item
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.EnumActionResult
import net.minecraft.util.EnumFacing
import net.minecraft.util.EnumHand
import net.minecraft.util.math.BlockPos
import net.minecraft.util.text.Style
import net.minecraft.util.text.TextComponentString
import net.minecraft.util.text.TextFormatting
import net.minecraft.world.World
import net.minecraftforge.fml.common.registry.GameRegistry

var itemDebugger = object: DLItem("debugger"){
    override fun onItemUse(player: EntityPlayer, world: World, pos: BlockPos, hand: EnumHand, facing: EnumFacing, hitX: Float, hitY: Float, hitZ: Float): EnumActionResult {
        if(world.isRemote) return EnumActionResult.SUCCESS

        val message = TextComponentString("")
        val tileEntity: TileEntity? = world.getTileEntity(pos)

        if(tileEntity == null) {
            message.appendText("No tileentity found!")
        }
        else {
            message.appendText("TileEntity: ")
            val tileEntityName = TextComponentString(tileEntity.javaClass.name)
            tileEntityName.style = Style().setColor(TextFormatting.LIGHT_PURPLE)
            message.appendSibling(tileEntityName)

            if(tileEntity is IDebuggable) {
                val debugHeader = TextComponentString("\n\nDEBUG INFO:\n\n")
                debugHeader.style = Style().setColor(TextFormatting.GRAY)
                message.appendSibling(debugHeader)
                message.appendSibling(tileEntity.debugInfo)
            }
        }

        player.sendMessage(message)

        return EnumActionResult.SUCCESS
    }
}.setCreativeTab()

fun initItems() {
    registerItem(itemDebugger)
}

fun registerItem(item: DLItem): DLItem {
    GameRegistry.register(item)
    item.registerItemModel()
    return item
}


open class DLItem(var name: String): Item() {
    init {
        unlocalizedName = name
        setRegistryName(modid, name)
    }

    fun registerItemModel() {
        proxy!!.registerItemRenderer(this, 0, this.name)
    }

    fun setCreativeTab(): DLItem {
        return this.setCreativeTab(dlCreativeTab)
    }

    override fun setCreativeTab(tab: CreativeTabs): DLItem {
        super.setCreativeTab(tab)
        return this
    }
}