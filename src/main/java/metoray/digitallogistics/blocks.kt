package metoray.digitallogistics

import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.block.state.IBlockState
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.ItemBlock
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.math.BlockPos
import net.minecraft.world.IBlockAccess
import net.minecraft.world.World
import net.minecraftforge.fml.common.registry.GameRegistry


var blockLapiron = object: DLBlock("lapiron", Material.IRON) {

    override fun isBeaconBase(worldObj: IBlockAccess?, pos: BlockPos?, beacon: BlockPos?): Boolean {
        return true
    }

}.setCreativeTab()


var blockConduit: BlockConduit = BlockConduit().setCreativeTab() as BlockConduit


fun initBlocks() {
    registerBlock(blockLapiron)
    registerBlock(blockConduit)
}


fun registerBlock(block: DLBlock, item: ItemBlock): DLBlock {
    GameRegistry.register(block)
    GameRegistry.register(item)
    block.registerItemModel(item)

    if(block is DLTEBlock<*>) {
        GameRegistry.registerTileEntity(
            block.getTileEntityClass(),
            block.registryName.toString()
        )
    }

    return block
}


fun registerBlock(block: DLBlock): DLBlock {
    val item: ItemBlock = ItemBlock(block)
    item.registryName = block.registryName
    return registerBlock(block, item)
}


open class DLBlock(var name: String, material: Material): Block(material) {
    init {
        unlocalizedName = name
        setRegistryName(modid, name)
    }

    fun registerItemModel(item: ItemBlock) {
        proxy!!.registerItemRenderer(item, 0, this.name)
    }

    fun setCreativeTab(): DLBlock {
        return this.setCreativeTab(dlCreativeTab)
    }

    override fun setCreativeTab(tab: CreativeTabs): DLBlock {
        super.setCreativeTab(tab)
        return this
    }
}

abstract class DLTEBlock<TE: TileEntity>(name: String, material: Material): DLBlock(name, material) {

    abstract fun getTileEntityClass(): Class<TE>

    fun getTileEntity(world: IBlockAccess, pos: BlockPos): TE? {
        var tileEntity = world.getTileEntity(pos)
        return world.getTileEntity(pos) as? TE
    }

    override fun hasTileEntity(state: IBlockState?): Boolean = true

    override abstract fun createTileEntity(world: World?, state: IBlockState?): TE
}