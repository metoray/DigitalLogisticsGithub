package metoray.digitallogistics

import com.google.common.collect.Lists
import net.minecraft.block.Block
import net.minecraft.block.BlockContainer
import net.minecraft.block.material.Material
import net.minecraft.block.properties.PropertyEnum
import net.minecraft.block.state.BlockStateContainer
import net.minecraft.block.state.IBlockState
import net.minecraft.client.renderer.block.model.ModelResourceLocation
import net.minecraft.client.renderer.block.statemap.StateMapperBase
import net.minecraft.entity.Entity
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.BlockRenderLayer
import net.minecraft.util.EnumFacing
import net.minecraft.util.EnumHand
import net.minecraft.util.IStringSerializable
import net.minecraft.util.math.AxisAlignedBB
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.RayTraceResult
import net.minecraft.util.math.Vec3d
import net.minecraft.util.text.TextComponentString
import net.minecraft.world.IBlockAccess
import net.minecraft.world.World
import net.minecraftforge.client.model.ModelLoader
import net.minecraftforge.common.property.ExtendedBlockState
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly


enum class EnumConnectionType(val solid: Boolean): IStringSerializable {
    NOT_CONNECTED(false),
    CONNECTED(true),
    EXTRACTING(true);

    override fun getName(): String? = this.toString().toLowerCase()
}


class BlockConduit: DLTEBlock<TileEntityConduit>("conduit", Material.CIRCUITS) {
    override fun getTileEntityClass(): Class<TileEntityConduit> {
        return TileEntityConduit::class.java
    }

    override fun createTileEntity(world: World?, state: IBlockState?): TileEntityConduit {
        return TileEntityConduit()
    }

    companion object {
        val DIRECTION_PROPERTIES: Array<PropertyEnum<EnumConnectionType>> = Array(
                EnumFacing.VALUES.size,
                {i -> PropertyEnum.create(
                        EnumFacing.VALUES[i].toString(),
                        EnumConnectionType::class.java)
                }
        )

        val SELECTED_BOUNDINGBOX = AxisAlignedBB(
                1.0/16.0*3, 1.0/16.0*3, 1.0/16.0*3,
                1.0/16.0*13, 1.0/16.0*13, 1.0/16.0*13
        )

        val AABB_CENTER = AxisAlignedBB(0.25, 0.25, 0.25, 0.75, 0.75, 0.75)
        val AABB_DIRECTIONS = Array(
                EnumFacing.VALUES.size,
                fun(i: Int): AxisAlignedBB {
                    val dir1 = Vec3d(EnumFacing.VALUES[i].directionVec)
                    val dir2 = Vec3d(EnumFacing.VALUES[(i + 2) % 6].directionVec).add(
                               Vec3d(EnumFacing.VALUES[(i + 4) % 6].directionVec)
                    )

                    val center = Vec3d(0.5, 0.5, 0.5)
                    val min = center.add(dir1.scale(0.5)).add(dir2.scale(0.25))
                    val max = center.add(dir1.scale(0.25)).add(dir2.scale(-0.25))

                    return AxisAlignedBB(min, max)
                }
        )
    }

    init {
        var defaultState = this.blockState.baseState
        for(property in BlockConduit.DIRECTION_PROPERTIES) {
            defaultState = defaultState.withProperty(property, EnumConnectionType.NOT_CONNECTED)
        }
        this.defaultState = defaultState
    }

    @SideOnly(Side.CLIENT)
    fun initModel() {
        ModelLoader.setCustomStateMapper(this, object : StateMapperBase() {
            override fun getModelResourceLocation(state: IBlockState?): ModelResourceLocation? {
                return ModelResourceLocation("$modid:custom/conduit")
            }
        })
    }

    @Suppress("OverridingDeprecatedMember")
    override fun isFullCube(state: IBlockState?): Boolean = false

    @Suppress("OverridingDeprecatedMember")
    override fun isOpaqueCube(state: IBlockState?): Boolean = false

    override fun getMetaFromState(state: IBlockState): Int = 0

    override fun getBlockLayer(): BlockRenderLayer? = BlockRenderLayer.CUTOUT

    @Suppress("OverridingDeprecatedMember")
    override fun getBoundingBox(state: IBlockState?, source: IBlockAccess?, pos: BlockPos?): AxisAlignedBB? = SELECTED_BOUNDINGBOX

    fun getConduitEntity(world: IBlockAccess, pos: BlockPos): TileEntityConduit? {
        val te = world.getTileEntity(pos)

        if(te !is TileEntityConduit) {
            DigitalLogistics.logger?.warn("Wrong tile entity at $pos")
            return null
        }
        else {
            return te
        }
    }

    override fun onBlockActivated(world: World, pos: BlockPos, state: IBlockState, player: EntityPlayer, hand: EnumHand, facing: EnumFacing, hitX: Float, hitY: Float, hitZ: Float): Boolean {
        val cside = EnumFacing.getFacingFromVector(hitX-.5f, hitY-.5f, hitZ-.5f)
        val item = player.getHeldItem(hand)
        val te = this.getConduitEntity(world, pos)

        if(item.item == itemDebugger) {
            return super.onBlockActivated(world, pos, state, player, hand, facing, hitX, hitY, hitZ)
        }

        player.sendMessage(TextComponentString("Activated block:\nfacing: $facing\nhit coords: $hitX, $hitY, $hitZ\nside?: $cside\nitem: $item\nempty: ${item.isEmpty}"))

        return true
    }

    override fun createBlockState(): BlockStateContainer {
        return ExtendedBlockState(
                this,
                DIRECTION_PROPERTIES,
                arrayOf()
        )
    }

    @Suppress("OverridingDeprecatedMember")
    override fun getActualState(state: IBlockState, worldIn: IBlockAccess, pos: BlockPos): IBlockState {
        return this.getExtendedState(state, worldIn, pos)
    }

    override fun getExtendedState(state: IBlockState, world: IBlockAccess, pos: BlockPos): IBlockState {
        var actualState = state
        val te = this.getConduitEntity(world, pos)

        for(direction in EnumFacing.values()) {
            val connectionType = te!!.connectionType(direction)
            val prop = BlockConduit.DIRECTION_PROPERTIES[direction.index]
            actualState = actualState.withProperty(
                    prop, connectionType
            )
        }
        return actualState
    }

    private fun getCollisionBoxList(bstate: IBlockState): List<AxisAlignedBB> {
        val list = Lists.newArrayList<AxisAlignedBB>()

        list.add(AABB_CENTER)

        for(facing in EnumFacing.VALUES) {
            val propval: Boolean = bstate.getValue(DIRECTION_PROPERTIES[facing.index]).solid
            if(propval) {
                list.add(AABB_DIRECTIONS[facing.index])
            }
        }

        return list
    }

    @Suppress("OverridingDeprecatedMember")
    override fun addCollisionBoxToList(state: IBlockState, worldIn: World, pos: BlockPos, entityBox: AxisAlignedBB, collidingBoxes: List<AxisAlignedBB>, entityIn: Entity?, someBool: Boolean) {
        var actualState = state
        if (!someBool) {
            actualState = this.getActualState(state, worldIn, pos)
        }

        for (axisalignedbb in getCollisionBoxList(actualState)) {
            Block.addCollisionBoxToList(pos, entityBox, collidingBoxes, axisalignedbb)
        }
    }

    @Suppress("OverridingDeprecatedMember")
    override fun collisionRayTrace(blockState: IBlockState, worldIn: World, pos: BlockPos, start: Vec3d, end: Vec3d): RayTraceResult? {
        val list = Lists.newArrayList<RayTraceResult>()

        for (axisalignedbb in getCollisionBoxList(this.getActualState(blockState, worldIn, pos))) {
            list.add(this.rayTrace(pos, start, end, axisalignedbb))
        }

        var raytraceresult1: RayTraceResult? = null
        var d1 = 0.0

        for (raytraceresult in list) {
            if (raytraceresult != null) {
                val d0 = raytraceresult.hitVec.squareDistanceTo(end)

                if (d0 > d1) {
                    raytraceresult1 = raytraceresult
                    d1 = d0
                }
            }
        }

        return raytraceresult1
    }
}
