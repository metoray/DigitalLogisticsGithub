package metoray.digitallogistics

import net.minecraft.init.Blocks
import net.minecraft.init.Items
import net.minecraft.item.EnumDyeColor
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.event.AnvilUpdateEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.fml.common.registry.GameRegistry

fun initRecipes() {
    GameRegistry.addRecipe(
            ItemStack(blockLapiron),
            "ILI",
            "LIL",
            "ILI",
            'I', Items.IRON_INGOT,
            'L', ItemStack(Items.DYE, 1, EnumDyeColor.BLUE.dyeDamage)
    )

    MinecraftForge.EVENT_BUS.register(object {
        @SubscribeEvent
        fun onAnvil(event: AnvilUpdateEvent) {
            if(event.right.item != Items.DIAMOND) return

            if(event.left.item != Item.getItemFromBlock(Blocks.IRON_BARS)) return

            event.cost = 1
            event.materialCost = 1
            event.output = ItemStack(Item.getItemFromBlock(blockConduit), event.left.count)
        }
    })
}