package metoray.digitallogistics

import metoray.digitallogistics.models.ConduitModelLoader
import net.minecraft.client.renderer.block.model.ModelResourceLocation
import net.minecraft.item.Item
import net.minecraftforge.client.model.ModelLoader
import net.minecraftforge.client.model.ModelLoaderRegistry

open class CommonProxy {
    open fun registerItemRenderer(item: Item, meta: Int, name: String) {}

    open fun preInit() {
    }

    open fun init() {
    }
}

class ClientProxy: CommonProxy() {
    override fun registerItemRenderer(item: Item, meta: Int, name: String) {
        val location = ModelResourceLocation("$modid:$name", "inventory")
        ModelLoader.setCustomModelResourceLocation(item, meta, location)
    }

    override fun preInit() {
        blockConduit.initModel()
        ModelLoaderRegistry.registerLoader(ConduitModelLoader())
    }

    override fun init() {
    }
}