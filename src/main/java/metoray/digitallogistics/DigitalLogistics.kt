package metoray.digitallogistics

import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.ItemStack
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.SidedProxy
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import org.apache.logging.log4j.Logger

const val modid: String = "digital_logistics"
const val version: String = "0.0.1"

@SidedProxy(
        serverSide = "metoray.digitallogistics.CommonProxy",
        clientSide = "metoray.digitallogistics.ClientProxy"
)
var proxy: CommonProxy? = null

@Mod(modid = modid, version = version)
class DigitalLogistics {

    companion object {
        // Mod logger, can be null if uninitialized
        var logger: Logger? = null
    }

    @Mod.EventHandler
    fun preInit(event: FMLPreInitializationEvent) {
        DigitalLogistics.logger = event.modLog
        initItems()
        initBlocks()
        proxy!!.preInit()
    }

    @Mod.EventHandler
    fun init(event: FMLInitializationEvent) {
        initRecipes()
        proxy!!.init()
    }

}

var dlCreativeTab = object : CreativeTabs(modid) {
    override fun getTabIconItem(): ItemStack {
        return ItemStack(blockLapiron)
    }

}