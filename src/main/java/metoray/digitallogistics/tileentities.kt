package metoray.digitallogistics

import metoray.digitallogistics.conduit_modules.ConduitConnectionModule
import net.minecraft.block.BlockContainer
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.nbt.NBTTagList
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.EnumFacing
import net.minecraft.util.ITickable
import net.minecraft.util.text.ITextComponent
import net.minecraft.util.text.Style
import net.minecraft.util.text.TextComponentString
import net.minecraft.util.text.TextFormatting
import net.minecraft.world.IWorldNameable
import java.util.*

//TODO: Optimize network updating

// Number of ticks before signal map is considered outdated
const val UPDATE_PERIOD: Int = 5

//Initial signal strength
const val SIGNAL_STRENGTH: Byte = 63

interface IDebuggable {
    val debugInfo: ITextComponent
}

class Signal(var name: String, var direction: EnumFacing?) {
    fun withDirection(direction: EnumFacing): Signal {
        return Signal(this.name, direction)
    }

    override fun toString(): String {
        return super.toString()
    }

    override fun equals(other: Any?): Boolean{
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Signal

        if (name != other.name) return false
        if (direction != other.direction) return false

        return true
    }

    override fun hashCode(): Int{
        var result = name.hashCode()
        result = 31 * result + (direction?.hashCode() ?: 0)
        return result
    }
}

class TileEntityConduit: TileEntity(), IDebuggable, ITickable {
    var lastUpdate: Long = 0
    var signalMap: Map<Signal, Byte> = HashMap()

    val signals: Map<Signal, Byte> get() {
        if(this.world.totalWorldTime - this.lastUpdate > UPDATE_PERIOD) {
            this.updateSignals()
        }
        return this.signalMap
    }

    fun updateSignals() {
        this.lastUpdate = this.world.totalWorldTime
        this.signalMap = this.calculateSignals()
        this.markDirty()
    }

    override fun update() {
        // Everything beyond this point happens on the server only
        if(this.world.isRemote) return
    }

    private fun calculateSignals(): Map<Signal, Byte> {
        val signalMap = this.localNodes

        for(direction in EnumFacing.VALUES) {
            val te: TileEntity? = this.world.getTileEntity(this.pos.offset(direction))

            if(te is TileEntityConduit) {
                for((signal, strength) in te.signals) {
                    if(signal.direction == direction.opposite || strength < 1) continue

                    val thisStrength = signalMap[signal]
                    val otherStrength: Byte = (strength - 1).toByte()

                    if(thisStrength == null || otherStrength > thisStrength) {
                        signalMap[signal.withDirection(direction)] = otherStrength
                    }
                }
            }
        }
        return signalMap
    }

    private val localNodes: HashMap<Signal, Byte> get() {
        val nodes = HashMap<Signal, Byte>()

        for(facing in EnumFacing.VALUES) {
            val pos = this.pos.offset(facing)
            val blockState = this.world.getBlockState(pos)

            if(blockState.block is BlockContainer) {
                val te = this.world.getTileEntity(pos)
                if(te is IWorldNameable)
                    nodes[Signal(te.name, facing)] = SIGNAL_STRENGTH
            }
        }
        return nodes
    }

    fun connectionType(facing: EnumFacing): EnumConnectionType {
        val state = world.getBlockState(pos.offset(facing))
        return if(state.block == blockConduit) {
            EnumConnectionType.CONNECTED
        }
        else if(state.block is BlockContainer) {
            EnumConnectionType.EXTRACTING
        }
        else {
            EnumConnectionType.NOT_CONNECTED
        }
    }

    override val debugInfo: ITextComponent get() {
        val message = TextComponentString("World time: ${this.world.totalWorldTime}\nLast update: ${this.lastUpdate}\n\n---- SIGNALS ----\n")

        for((signal, strength) in this.signals) {
            val sigText = TextComponentString("${signal.direction} [$strength]: ")
            val nodeText = TextComponentString("${signal.name}\n")
            nodeText.style = Style().setColor(TextFormatting.AQUA)

            message.appendSibling(sigText)
            message.appendSibling(nodeText)
        }

        message.appendText("\n")

        val nbt = this.serializeNBT()
        message.appendText(nbt.toString())

        return message
    }

    override fun writeToNBT(compound: NBTTagCompound): NBTTagCompound {
        val nbt = super.writeToNBT(compound)
        nbt.setLong("last_update", this.lastUpdate)

        val signals = NBTTagList()

        for((signal, strength) in this.signalMap) {
            val signalNbt = NBTTagCompound()
            signalNbt.setString("name", signal.name)
            signalNbt.setString("direction", signal.direction?.name ?: "")
            signalNbt.setByte("signal", strength)
            signals.appendTag(signalNbt)
        }

        nbt.setTag("signals", signals)
        return nbt
    }

    override fun readFromNBT(nbt: NBTTagCompound) {
        super.readFromNBT(nbt)

        //TODO: Replace magic number 10 which represents a nbttagcompound
        val signals = nbt.getTagList("signals", 10)
        val signalMap = HashMap<Signal, Byte>()

        for(i in 0 until signals.tagCount()) {
            val signalNBT = signals.getCompoundTagAt(i)
            val name = signalNBT.getString("name")
            val direction = EnumFacing.byName(signalNBT.getString("direction"))
            val strength = signalNBT.getByte("signal")
            signalMap[Signal(name, direction)] = strength
        }

        this.signalMap = signalMap
        this.lastUpdate = nbt.getLong("last_update")
    }
}