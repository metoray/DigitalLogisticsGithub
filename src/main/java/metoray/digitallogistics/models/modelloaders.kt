package metoray.digitallogistics.models

import metoray.digitallogistics.modid
import net.minecraft.client.resources.IResourceManager
import net.minecraft.util.ResourceLocation
import net.minecraftforge.client.model.ICustomModelLoader
import net.minecraftforge.client.model.IModel

class ConduitModelLoader: ICustomModelLoader {
    companion object {
        val AVAILABLE_MODELS = arrayOf("custom/conduit")
        val CONDUIT_MODEL = ConduitModel()
    }

    override fun loadModel(modelLocation: ResourceLocation): IModel? {
        return when(modelLocation.resourcePath) {
            "custom/conduit" -> CONDUIT_MODEL
            else -> null
        }
    }

    override fun accepts(modelLocation: ResourceLocation): Boolean {
        if(modelLocation.resourceDomain != modid) {
            return false
        }

        return modelLocation.resourcePath in AVAILABLE_MODELS
    }

    override fun onResourceManagerReload(resourceManager: IResourceManager?) {

    }

}