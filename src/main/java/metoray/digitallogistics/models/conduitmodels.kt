package metoray.digitallogistics.models

import com.google.common.base.Function
import com.google.common.collect.ImmutableSet
import metoray.digitallogistics.BlockConduit
import metoray.digitallogistics.EnumConnectionType
import metoray.digitallogistics.modid
import net.minecraft.block.state.IBlockState
import net.minecraft.client.renderer.block.model.BakedQuad
import net.minecraft.client.renderer.block.model.IBakedModel
import net.minecraft.client.renderer.block.model.ModelRotation
import net.minecraft.client.renderer.texture.TextureAtlasSprite
import net.minecraft.client.renderer.vertex.VertexFormat
import net.minecraft.util.EnumFacing
import net.minecraft.util.ResourceLocation
import net.minecraftforge.client.model.IModel
import net.minecraftforge.client.model.ModelLoaderRegistry
import net.minecraftforge.common.model.IModelState
import net.minecraftforge.common.model.TRSRTransformation
import java.util.*

class DirectionalSubmodel(
        location: ResourceLocation,
        state: TRSRTransformation,
        format: VertexFormat,
        bakedTextureGetter: Function<ResourceLocation, TextureAtlasSprite>
) {

    val models = EnumMap<EnumFacing, IBakedModel>(EnumFacing::class.java)

    init {
        val normalModel = ModelLoaderRegistry.getModel(location)

        for(direction in EnumFacing.VALUES) {
            val rotation = when(direction) {
                EnumFacing.DOWN -> ModelRotation.X0_Y0
                EnumFacing.UP -> ModelRotation.X180_Y0
                EnumFacing.NORTH -> ModelRotation.X90_Y180
                EnumFacing.SOUTH -> ModelRotation.X90_Y0
                EnumFacing.WEST -> ModelRotation.X90_Y90
                EnumFacing.EAST -> ModelRotation.X90_Y270
            }

            val rotatedState = state.compose(TRSRTransformation(rotation))

            models[direction] = normalModel.bake(rotatedState, format, bakedTextureGetter)
        }
    }
}

class ConduitModel: IModel {
    override fun bake(state: IModelState,
                      format: VertexFormat,
                      bakedTextureGetter: Function<ResourceLocation, TextureAtlasSprite>
    ): IBakedModel {
        var modifiedState = TRSRTransformation(ModelRotation.X90_Y0)

        modifiedState = (state as TRSRTransformation).compose(modifiedState)

        val mainModel = ModelLoaderRegistry.getModel(
                ResourceLocation("$modid:block/conduit")
        ).bake(modifiedState, format, bakedTextureGetter)

        val parts = EnumMap<EnumConnectionType, DirectionalSubmodel>(EnumConnectionType::class.java)
        for(conduitPart in EnumConnectionType.values()) {
            val partName = conduitPart.toString().toLowerCase()
            val location = ResourceLocation(modid, "conduit_part/$partName")
            parts[conduitPart] = DirectionalSubmodel(location, state, format, bakedTextureGetter)
        }

        return BakedConduitModel(mainModel, parts)
    }

    override fun getTextures(): MutableCollection<ResourceLocation>? {
        return ImmutableSet.of(
                ResourceLocation(modid, "blocks/active_conduit"),
                ResourceLocation(modid, "blocks/rough_metal")
        )
    }

    override fun getDefaultState(): IModelState? {
        return TRSRTransformation.identity()
    }

    override fun getDependencies(): MutableCollection<ResourceLocation>? {
        return Collections.emptySet()
    }

}

class BakedConduitModel(var mainModel: IBakedModel,
                        var submodels: Map<EnumConnectionType, DirectionalSubmodel>
): IBakedModel by mainModel {

    override fun getQuads(state: IBlockState?, side: EnumFacing?, rand: Long): MutableList<BakedQuad>? {
        if(side != null) return Collections.emptyList()

        val quads = ArrayList<BakedQuad>()
        quads.addAll(this.mainModel.getQuads(state, side, rand))

        for((property, direction: EnumFacing) in BlockConduit.DIRECTION_PROPERTIES.zip(EnumFacing.VALUES)) {
            val submodel: DirectionalSubmodel? = this.submodels[state!!.getValue(property)]

            if(submodel == null) continue

            val subQuads: MutableList<BakedQuad> = submodel.models[direction]!!.getQuads(state, side, rand)
            quads.addAll(subQuads)
        }

        return quads
    }

}